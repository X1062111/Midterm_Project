# Software Studio 2018 Spring Midterm Project
## Notice
* Replace all [xxxx] to your answer

## Topic
* Forum
* Key functions (add/delete)
    1. user page,    
    2. post page,
    3. post list page,
    4. leave comment under any post 

* Other functions (add/delete)
    1. userpage可以显示该用户所有回复（不只是贴文）
    2. userpage可以显示所点击用户的信息，当此用户为自己时，可以对自己的一些信息进行更改
    

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|20%|N|    
|GitLab Page|5%|N|              
|Database|15%|N|                 
|RWD|15%|N|                     
|Topic Key Function|15%|N|      

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|N|    
|Chrome Notification|5%|N|      
|Use CSS Animation|2.5%|N|
|Security Report|5%|N| 
|Other functions|1~10%|N|

## Website Detail Description

1.登录：使用邮箱注册账号与登录（每个邮箱只能注册一个账号）;使用Google账号登录
2.主页浏览所有贴文，可点击标题进入详情页进行评论 ;可点击作者账号 进入作者的userpage了解此作者信息与其他贴文和评论
3.为方便用户评论，在详情页点击comment按钮可直接跳转至评论框
4.在自己的userpage ，可修改个人信息和个人简介，以及浏览自己的历史贴文与回复。
5.使用的bootstrap的部分component和Lab06的template；
6.左上角的BME 点击可返回主页
## Security Report (Optional)

1.每台设备同时只能登陆一个账号，且多个子标签页间的用户状态保持同步；
2.与数据相关的html大多通过js以字符串形式生成，避免恶意注入修改盗取数据；
3.用户必须登录才能阅读论坛内容及其他操作

作业网站：https://xjgmidtermproject.firebaseapp.com/