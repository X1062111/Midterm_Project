
function init() {
    var user_email = '';
    firebase.auth().onAuthStateChanged(function (user) {
        var menu = document.getElementById('dynamic-menu');
        // Check user login
        if (user) {
            user_email = user.email;
            menu.innerHTML = "<span class='dropdown-item'><a href='userpage.html?uid="+user.email+"'>" + user.email+ "</a></span><span class='dropdown-item' id='logout-btn'>Logout</span>";
           
            var logOut = document.getElementById('logout-btn');
            logOut.addEventListener('click', e => {
                firebase.auth().signOut().then(function () {
                   alert("Logout Success！");
                }).catch(e => alert("Logout error！"));
            });
        } else {
            // It won't show any post if not login
            menu.innerHTML = "<a class='dropdown-item' href='signin.html'>Login</a>";
            document.getElementById('post_list').innerHTML = "";
        }
    });

    post_btn = document.getElementById('post_btn');
    post_txt = document.getElementById('comment');
    post_title = document.getElementById('title');
    
    post_btn.addEventListener('click', function () {
        if (post_txt.value != "") {
            if(post_title.value ==""){
                alert("please input title！");
            }
            else{
                /// TODO 6: Push the post to database's "com_list" node
                ///         1. Get the reference of "com_list"
                ///         2. Push user email and post data
                ///         3. Clear text field
                var date = new Date();
                var postTime = date.getFullYear()+"-"+ (date.getMonth()+1)+"-"+date.getDate()+" "+date.getHours()+":"+date.getMinutes()+":"+date.getSeconds();
                var newPostRef= firebase.database().ref('com_list').push();
                newPostRef.set({
                    "user_email":user_email,
                    "text":post_txt.value,
                    "title":post_title.value,
                    "time":postTime,
                    
                });
                post_txt.value = "";
                post_title.value = "";
               
                console.log(postId);
            }
        }
    });

    // The html code for post

    var str_before_title = "<div class='my-3 p-3 bg-white rounded box-shadow'><div class='border-bottom border-gray pb-2 mb-0 '><span ><a class='postlink' href='topic.html?id="
    //var str_after_date="</h6><h6 class='border-bottom border-gray pb-2 mb-0 '>"; //after date before title
    var str_in_title = "' target='_blank'>";
    var str_after_title="</a></span><span class='float-right text-align-right'>";
    var str_after_date="</span></div><div class='media text-muted pt-3'><img src='img/default.svg' alt='' class='mr-2 rounded' style='height:32px; width:32px;'><p class='media-body pb-3 mb-0 small lh-125 border-bottom border-gray'><a class='d-block text-gray-dark uidlink' href='userpage.html?uid=";
    var str_after_content = "</p></div></div>\n";
    var postsRef = firebase.database().ref('com_list');
    // List for store posts html
    var total_post = [];
    // Counter for checking history post update complete
    var first_count = 0;
    // Counter for checking when to update new post
    var second_count = 0;

    postsRef.once('value')
        .then(function (snapshot) {
            snapshot.forEach(function(childSnapshot){
                var childData = childSnapshot.val();
                total_post[total_post.length]=str_before_title+childSnapshot.key+str_in_title+childData.title+str_after_title+childData.time+str_after_date+childData.user_email+"' target='_blank'>"+childData.user_email+"</a>"+childData.text+str_after_content;
               // total_post[total_post.length]=str_before_username+childData.user_email+"</strong>"+childData.text+str_after_content;
                first_count +=1;
               
            });
            document.getElementById('post_list').innerHTML=total_post.join('');

            postsRef.on('child_added',function(data){
                second_count +=1;
                if(second_count>first_count){
                    var childData = data.val();
                    total_post[total_post.length]=str_before_title+data.key+str_in_title+childData.title+str_after_title+childData.time+str_after_date+childData.user_email+"' target='_blank'>"+childData.user_email+"</a>"+childData.text+str_after_content;
                    document.getElementById('post_list').innerHTML=total_post.join('');
                    console.log(data.key)
                }
            });
            /// TODO 7: Get all history posts when the web page is loaded and add listener to update new post
            ///         1. Get all history post and push to a list (str_before_username + email + </strong> + data + str_after_content)
            ///         2. Join all post in list to html in once
            ///         4. Add listener for update the new post
            ///         5. Push new post's html to a list
            ///         6. Re-join all post in list to html when update
            ///
            ///         Hint: When history post count is less then new post count, update the new and refresh html
            
        })
        .catch(e => console.log(e.message));
}

window.onload = function () {
    init();
};