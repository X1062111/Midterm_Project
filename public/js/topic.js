function init() {
    var user_email = '';
    firebase.auth().onAuthStateChanged(function (user) {
        var menu = document.getElementById('dynamic-menu');
        // Check user login
        if (user) {
            user_email = user.email;
            menu.innerHTML = "<span class='dropdown-item'><a href='userpage.html?uid="+user.email+"'>" + user.email + "</a></span><span class='dropdown-item' id='logout-btn'>Logout</span>";
            
            var logOut = document.getElementById('logout-btn');
            logOut.addEventListener('click', e => {
                firebase.auth().signOut().then(function () {
                   alert("Logout Success！");
                }).catch(e => alert("Logout error！"));
            });
        } else {
            // It won't show any post if not login
            menu.innerHTML = "<a class='dropdown-item' href='signin.html'>Login</a>";
            document.getElementById('post_list').innerHTML = "";
        }
    });

    function keySearch() {
        var str=location.href; //取得整个地址栏
        var num=str.indexOf("=");
        var num2=str.indexOf("#",num);
        if(num2 == -1){
            return str.substr(num+1);
        }
        else{
            return str.substr(num+1,num2-num-1);
        }
    }

    var post = document.getElementById("post");
    var before_post_title = "<h4 class='border-bottom border-gray pb-2 mb-0'>";//+
    var after_post_title = "</h4><small class='writerAndTime d-block'><span class='float-left margin-right-small'><a class='d-inline text-gray-dark uidlink' href='userpage.html?uid="
    var after_writer= "</span></small><div class='row d-block'><div class='col-sm-12'><p class='d-block border-bottom border-gray pb-2 mb-0'>";
    var after_post_text = "</p></div></div><div class='media text-muted pt-3'><a href='#bottom' class='btn btn-info btn-sm active' role='button'target='_self'>Comment</a></div>"

    var key= keySearch();                                //从url中获取所点击的帖文的id
    var postTitle = "";
    console.log(key);
    var postRef= firebase.database().ref('com_list/'+key);  //根据id获取目标帖文的路径

    postRef.once('value').then(function(snapshot){             //获取目标帖文的数据并填入对应div
       let data = snapshot.val();
       postTitle = data.title;
       post.innerHTML=before_post_title+data.title+after_post_title+data.user_email+"' target='_blank'>"+data.user_email+"</a></span>&nbsp&nbsp<span class='float-left'>"+data.time+after_writer+data.text+after_post_text;

    })                                             
    
    var jumpToCom = document.getElementById('')
    comment_btn.addEventListener('click', function () {
    });

    ///将评论框中提交的内容存入数据库
    comment_btn = document.getElementById('comment_btn');
    comment_txt = document.getElementById('comment');
    comment_btn.addEventListener('click', function () {
        if (comment_txt.value != "") {
                var user_comment=comment_txt.value;
                var date = new Date();
                var commentTime = date.getFullYear()+"-"+ (date.getMonth()+1)+"-"+date.getDate()+" "+date.getHours()+":"+date.getMinutes()+":"+date.getSeconds();
                console.log(commentTime);
                var newComRef= firebase.database().ref(key).push();
                newComRef.set({
                    "user_email":user_email,
                    "text":comment_txt.value,
                    "time":commentTime,
                    
                });
                

                var usersRef= firebase.database().ref("users");
                var findTheUser = false;             //判断是否在users中找到此用户
                usersRef.once('value').then(function (snapshot){
                    snapshot.forEach(function(child){
                        const someone = child.val();
                        if(someone.user_email == user_email )
                        {
                          findTheUser= true;
                          var ukey = child.key;
                          var currentUserRef = firebase.database().ref("users/"+ukey+"/comment").push();
                          currentUserRef.set({
                              "text":user_comment,
                              "time":commentTime,
                              "post_title":postTitle,
                              "post_key":key,  
                            })                          
                        }
                    });
                    if(findTheUser==false){
                        var newUsersKey= firebase.database().ref("users").push().key;
                        firebase.database().ref("users/"+newUsersKey).set({
                            "user_email":user_email,
                        });
                        var newUserComRef = firebase.database().ref("users/"+newUsersKey+"/comment").push();
                        newUserComRef.set({
                              "text":user_comment,
                              "time":commentTime,
                              "post_title":postTitle,
                              "post_key":key,
                        })
                    }
                });
        }
        comment_txt.value = "";
    });

    //************************************************************ */
    var commentsRef = firebase.database().ref(key);
    // List for store posts html
    var total_comment = [];
    // Counter for checking history post update complete
    var first_count = 0;
    // Counter for checking when to update new post
    var second_count = 0;

   
    var str_before_commenter = "<div class='my-3 p-3 bg-white rounded box-shadow'><div class='media text-muted pt-3'><img src='img/default.svg' alt='' class='mr-2 rounded' style='height:32px; width:32px;'><div style='width:100%'><div style='background-color:#d2e5fc '><span class='commenter_id'><a class='d-inline text-gray-dark uidlink' href='userpage.html?uid="
    var between_Com_Time = "</a>&nbsp&nbsp</span><span class='text-gray-dark float-right text-align-right'>"
    var str_after_time = "</span></div><p>";
    var str_after_content = "</p></div></div></div>\n";
    commentsRef.once('value')                         
        .then(function (snapshot) {
            snapshot.forEach(function(childSnapshot){
                var childData = childSnapshot.val();
                total_comment[total_comment.length]=str_before_commenter+ childData.user_email +"' target='_blank'>"+childData.user_email+between_Com_Time+childData.time+str_after_time+childData.text+str_after_content;
              
                first_count +=1;
               
            });
            document.getElementById('comment_list').innerHTML=total_comment.join('');

            commentsRef.on('child_added',function(data){
                second_count +=1;
                if(second_count>first_count){
                    var childData = data.val();
                    total_comment[total_comment.length]=str_before_commenter+ childData.user_email +"' target='_blank'>"+childData.user_email+between_Com_Time+childData.time+str_after_time+childData.text+str_after_content;
                    document.getElementById('comment_list').innerHTML=total_comment.join('');
                    console.log(data.key)
                }
            });
        })
        .catch(e => console.log(e.message));
}





window.onload = function () {
    init();

};
