function init(user_email) {
    var user_email = 'n';
    var person = ''; //当前页面显示的信息的所属人

    firebase.auth().onAuthStateChanged(function (user) {
        var menu = document.getElementById('dynamic-menu');
        // Check user login
        if (user) {
           user_email=user.email;
            menu.innerHTML = "<span class='dropdown-item'><a id='accountId' href='userpage.html?uid="+user.email+"'>" + user.email + "</a></span><span class='dropdown-item' id='logout-btn'>Logout</span>";
            
            var logOut = document.getElementById('logout-btn');
            logOut.addEventListener('click', e => {
                firebase.auth().signOut().then(function () {
                   alert("Logout Success！");
                }).catch(e => alert("Logout error！"));
            });
            
            function userSearch() {
                var str=location.href; //取得整个地址栏 目的是得知此页面该显示哪位用户的信息
                var num=str.indexOf("=");
                var num2=str.indexOf("#",num);                                                                   
                if(num2 == -1){
                    return  str.substr(num+1);
                }
                else{
                    return str.substr(num+1,num2-num-1);
                }
                //console.log(str.substr(num+1));
            }
            
            person=userSearch();
            console.log(person,user_email,person==user_email);
            if(person==user_email)                                              // 说明用户点击的是自己的信息 ，此时应允许用户更改自己的信息
                {
                    document.getElementById("editresume_btn").disabled = false;
                    document.getElementById("editname_btn").disabled = false;
                    document.getElementById("First name").disabled=false; 
                    document.getElementById("Last name").disabled=false; 
                    document.getElementById("resume").disabled=false; 
                }

        //-------------------加载用户信息
            var firstname = document.getElementById("First name");           //为从数据库载出信息或向数据库写入信息做准备
            var lastname = document.getElementById("Last name");
            var resume = document.getElementById("resume");
            var editresume_btn = document.getElementById("editresume_btn");
            var editname_btn = document.getElementById("editname_btn");
            
            var userInfoRef= firebase.database().ref("users");                  ////////////////
            var userKey = null;
            

            editname_btn.addEventListener('click', function () {
                if (firstname.value != "" && lastname.value != "") {
                    var findInfoname = false;
                    userInfoRef.once('value').then(function (snapshot){
                        snapshot.forEach(function(child){
                            var onesInfo = child.val();
                            if(onesInfo.user_email == person ){
                                console.log(onesInfo.user_email,person,onesInfo.user_email == person);
                                findInfoname=true;
                                var ukey = child.key;
                                var updates = {};
                                updates['/firstname'] = firstname.value;
                                updates['/lastname'] = lastname.value;
                                firebase.database().ref("users/"+ukey).update(updates);
                            }
                        })
                        if(findInfoname==false){
                            firebase.database().ref("users").push()
                            var newUsersKey= firebase.database().ref("users").push().key;
                            firebase.database().ref("users/"+newUsersKey).set({
                                "user_email":user_email,
                                "firstname":firstname.value,
                                "lastname":lastname.value,
                            });
                        }
                        
                    });
                    alert("Update Success!");     
                }
            });
        
            editresume_btn.addEventListener('click', function () {
                if (resume.innerHTML != "" ) {
                    var findInfocv = false;
                    userInfoRef.once('value').then(function (snapshot){
                        snapshot.forEach(function(child){
                            var onesInfo = child.val();
                            if(onesInfo.user_email == person ){
                                findInfocv=true;
                                var ukey = child.key;
                                var updates = {};
                                updates['/resume'] = resume.value;
                                console.log("1",resume.value);
                                
                                firebase.database().ref("users/"+ukey).update(updates);                          
                            }
                        })
                        if(findInfocv==false){
                            firebase.database().ref("users").push()
                            var newUsersKey= firebase.database().ref("users").push().key;
                            firebase.database().ref("users/"+newUsersKey).set({
                                "user_email":user_email,
                                "resume":resume.innerHTML,
                            });
                        }
                        
                    });
                    alert("Update Success!"); 
                }
            });
        
                                                               //加载信息  
            var tryfillInfo = false;
                    userInfoRef.once('value').then(function (snapshot){
                        snapshot.forEach(function(child){
                            var onesInfo = child.val();
                            if(onesInfo.user_email == person ){
                                tryfillInfo=true;
                                if(onesInfo.firstname){
                                    firstname.value=onesInfo.firstname;
                                    lastname.value=onesInfo.lastname;
                                }
                                else{
                                    firstname.value="Firstname";
                                    lastname.value="Lastname";
                                }
                                if(onesInfo.resume){
                                    
                                    resume.innerHTML=onesInfo.resume;
                                }
                                else{
                                    resume.innerHTML="say nothing...";
                                }
                            }
                        })
                        if(tryfillInfo==false){
                            firstname.value="...";
                            lastname.value="...";
                            resume.innerHTML="......";
                        }
                        
                    });             
////-----------------------------------------------------------------------------------

            var post_before_title = "<div class='my-3 p-3  border-bottom-line'><div class='border-bottom border-gray pb-2 mb-0 row'><div class='col-sm-6'><a class='postlink' href='topic.html?id="
          
            var post_in_title = "' target='_blank'>";
            var post_after_title="</a></div><div class='col-sm-6 text-align-right '>";
            var post_after_date="</div></div><div class='media text-muted pt-3 row'><div class='col-sm-12'><p class='media-body pb-3 mb-0 small lh-125 border-bottom border-gray'>";
            var post_after_content = "</p></div></div></div>\n";
            var total_post = [];

           

            var post_history = document.getElementById("post_history");

            var postsRef = firebase.database().ref('com_list');
            postsRef.once('value').then(function (snapshot){
                snapshot.forEach(function(child){
                  const post = child.val();
                  if(post.user_email == person )
                  {
                      console.log(post.title);
                      total_post[total_post.length]=post_before_title+child.key+post_in_title+post.title+post_after_title+post.time+post_after_date+post.text+post_after_content;
                     
                  }
                });
                post_history.innerHTML=total_post.join(''); 
            });

            //My comment ///////////////////////////////////

            var comment_before_title = "<div class='my-3 p-3  border-bottom-line'><div class='border-bottom border-gray pb-2 mb-0 row'><div class='col-sm-6'><a class='postlink' href='topic.html?id="
          
            var comment_in_title = "' target='_blank'>";
            var comment_after_title="</a></div><div class='col-sm-6 text-align-right'>";
            var comment_after_date="</div></div><div class='media text-muted pt-3 row'><div class='col-sm-12'><p class='media-body pb-3 mb-0 small lh-125 border-bottom border-gray'>";
            var comment_after_content = "</p></div></div></div>\n";
            var total_comment = [];
            var comment_history = document.getElementById("comment_history");

            var personRef = firebase.database().ref('users');
            personRef.once('value').then(function (snapshot){
                snapshot.forEach(function(child){               //遍历所有用户
                    var somebody = child.val();
                    if(somebody.user_email == person )            //找到当前要查询的用户
                    {
                        firebase.database().ref('users/'+child.key+'/comment').once('value').then(function (comSnapshot){     //对此用户的所有评论进行遍历
                            comSnapshot.forEach(function(comChild){
                                var oneComment = comChild.val();

                                total_comment[total_comment.length]=comment_before_title+oneComment.post_key+comment_in_title+oneComment.post_title+comment_after_title+oneComment.time+comment_after_date+oneComment.text+comment_after_content;

                            });
                            comment_history.innerHTML=total_comment.join(''); 
                        });
                    }
                });  
            })
           
            //-------------------------------------        
        } else {
            // It won't show any post if not login
            menu.innerHTML = "<a class='dropdown-item' href='signin.html'>Login</a>";
            document.getElementById('post_list').innerHTML = "";
        }
        
    });
    console.log(user_email,"2");
 

}


window.onload = function () {
    
    init();
   

};
